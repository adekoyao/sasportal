'use strict';

routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    $stateProvider
        .state('landingpage', {
            url: '/landingpage',
            template: require('./landingpage.html'),
            controller: 'LandingPageController',
            controllerAs: 'landingpage'
        });
}