'use strict';

import angular from 'angular';
import uirouter from 'angular-ui-router';
import '../landingpage/landingpage.scss';
import '../landingpage/w3.css';
import routing from './landingpage.routes';
import LandingPageController from './landingpage.controller';
import mainMenu from '../../directives/js/main-menu';
import menubar from '../../directives/js/menubar';
import appServices from '../../factories/appServices';

export default angular.module('app.landingpage', [uirouter, menubar, mainMenu, appServices])
    .config(routing)
    .controller('LandingPageController', LandingPageController)
    .name;