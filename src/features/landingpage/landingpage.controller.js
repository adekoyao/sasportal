'use strict';



import assesspic from '../../directives/imgs/assesspic.jpg';
import qualyspic from '../../directives/imgs/qualys.png';









export default function LandingPageController($scope ,$state, $stateParams, $timeout, appServices ) {

    $scope.qualyspic = qualyspic;

    $scope.myApps = function(){
        $state.go('review-app', {username :$scope.user });
    };






    $scope.dashboard = function(){
        $state.go('current-assessment');
    };

    $scope.user = appServices.getUserName();
    $scope.appDashboard = function(){
        $state.go('review-app', {username :$scope.user });
    };
    $scope.newApplication = function(){
        $state.go('define-app', {username :$scope.user });
    };
    $scope.manageQualysScan = function(){
        $state.go('qualysScan', {username :$scope.user });
    };

    $scope.textholder1="Review and modify existing application information for your SAS service" ;
    $scope.textholder2="Create a new application for your SAS service" ;
    $scope.textholder3="Upload new scan reports and map ASM based mitigation controls" ;

    $scope.hovering = false;

    // create the timer variable
    var timer;

    // mouseenter event
    $scope.showIt = function () {
        $timeout.cancel(timer);
        $scope.hovering = true;
    };

    // mouseleave event
    $scope.hideIt = function () {
        timer = $timeout(function () {
            $scope.hovering = false;
        }, 750);
    };
//end

}

LandingPageController.$inject = ['$scope', '$state', '$stateParams', '$timeout', 'appServices'];
