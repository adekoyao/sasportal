'use strict';

import assesspic from '../../directives/imgs/assesspic.jpg';


export default function HomePageController($scope ,$state, $stateParams, appServices ) {

    $scope.assesspic = assesspic;
    $scope.myApps = function(){
        $state.go('review-app', {username :$scope.user });
    };
    $scope.define = function(){
        $state.go('define-app', {username :$scope.user });
    };
    $scope.manageXML = function(){
        $state.go('qualysScan', {username :$scope.user });
    };
    $scope.dashboard = function(){
        $state.go('current-assessment');
    };
    $scope.user = appServices.getUserName();



    $scope.Addnumb =function(numb1,numb2)
    {
        //$scope.result=parseInt(numb1) + parseInt(numb2);
        $scope.result=numb1 + numb2;
    }







}
HomePageController.$inject = ['$scope', '$state', '$stateParams', 'appServices'];
